package ramasgit;

public class Ramas5 {

	public static void main(String[] args) {
		
		// Declaraci�n e inicializaci�n de un array de enteros de tama�o 10
		
		int enteros[]= new int[10];
		
		//Introducci�n de valores en el array y visualizaci�n de los mismos: en cada posici�n el contenido ser� su n�mero de posici�n
		
		for (int i = 0; i < enteros.length; i++) {
			enteros[i]= i;
			System.out.println("posicion " + i + ": de valor " + enteros[i]);
		}
		//Modificaci�n del array y visualizaci�n del mismo: en las posiciones pares del array se modificar� el contenido que tenga por 0
		
		for (int i = 0; i < enteros.length; i += 2) {
			enteros[i] = 0;
		}
		
		//Visualizaci�n del n�mero de ceros que hay en el array.
		
		for (int i : enteros) {
			if (i == 0) {
				System.out.println("numero de ceros"+ i);
			}
		}
		//Visualizaci�n del n�mero de elementos distintos de cero que hay en el array.
		for (int i : enteros) {
			if(i > 0) {
				System.out.println("numeros diferentes de cero" + i);
			}
		}

	}

}
